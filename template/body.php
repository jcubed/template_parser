<?php
/**
 * Lavet af Patrick Jørgensen
 * Template til Body
 *
 */

?>
<main role="main">
      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <div class="container">
          <h1 class="display-3"><?php echo get_text('text', 'hello') . get_text('person', 'name'); ?></h1>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-primary btn-lg" href="#" role="button"><?php echo get_text('text', 'learnMore') ?> </a></p>
        </div>
      </div>

      <div class="container">
        <!-- Example row of columns -->
        <div class="row">
          <?php for($i=0;$i<3;$i++) {?>
          <div class="col-md-4">
            <h2><?php echo get_text('text', 'headline') ?></h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-secondary" href="#" role="button"><?php echo get_text('text', 'details') ?> </a></p>
          </div>
          <?php } ?>
        </div>

        <hr>

      </div> <!-- /container -->
</main>