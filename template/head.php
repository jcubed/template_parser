<?php
/**
 * Lavet af Patrick Jørgensen
 * Template til Head
 *
 */

?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html><head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" 
  integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  <title>PHP Eksempel</title>
  </head><body>

  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="#"><?php echo get_text('nav', 'logo') ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#"><?php echo get_text('nav', 'one') ?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><?php echo get_text('nav', 'two') ?></a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="text" placeholder="<?php echo get_text('nav', 'search') ?>" aria-label="<?php echo get_text('nav', 'search') ?>">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><?php echo get_text('nav', 'search') ?></button>
        </form>
      </div>
    </nav>