<?php

/* Lavet af Patrick Jørgensen */

mb_internal_encoding("UTF-8");
mb_http_output( "UTF-8" );
ob_start("mb_output_handler");

class std {
	/* Constructor function til at generere nye std objekter */ 
    public function __construct(array $arguments = array()) {
        if (!empty($arguments)) {
            foreach ($arguments as $property => $argument) {
                $this->{$property} = $argument;
            }
        }
    }

    /* Call function til at hente gemte variabler i array af objekt */
    public function __call($method, $arguments) {
        $arguments = array_merge(array("std" => $this), $arguments);
        if (isset($this->{$method}) && is_callable($this->{$method})) {
            return call_user_func_array($this->{$method}, $arguments);
        } else {
            throw new Exception("Fatal error: Call to undefined method std::{$method}()");
        }
    }
}


?>